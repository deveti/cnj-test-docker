# Local docker env setup

# Requirements

Download Docker client for your environment : https://www.docker.com/

# Config

## 1. Generate SSL certificate for nginx
`openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout C:\Projects\docker-cert.key -out C:\Projects\docker-cert.crt`

Change `C:\Projects\` with your path. Follow the prompt and when asked to enter `Common Name` insert domain name you are going to use as local environtment. Example: `project.local`.

## 2. Copy certificates and set nginx hostnames
Copy generated certificates in `src/proxy/resources/certs`. Overwrite existing certificates.

Edit `src/proxy/resources/default-development.conf` and change `project.local` to hostname you used in step 1.

Edit `src/nginx/resources/default-development.conf` and change `project.local` variable to hostname you used in step 1.

# Setup

## 1. Shut down old docker environment

shut down any currently running docker containers:
`docker-compose down`

docker ps should return empty:
`docker ps`

Remove old containers:
`docker system prune -a`

## 2. Clone the repository 
`git@gitlab.com:simpliko/common/docker.git your-project-folder`

## 3. Update docker .env file
```
CODE_PATH_HOST={PATH} - Relative path to project (../code)
```

## 4. Copy credentials to .env file
Copy **database** credentials **from docker .env file** to your **project .env file**.

## 5. Add hosts
Add these records to your hosts file (change project.local with your project hostname):
```
127.0.0.1 project.local
```

## 6. SSL certificates

1. Go to **src/proxy/resources/certs/**
2. Add all **.cert** files to your system (keychain)
3. Set certificates to always trust

# Running the environment

Commands
```
Docker:
docker-compose up
docker-compose up -d (run as a daemon)
```

# Refreshing the docker environment
If you need to refresh the docker containers for any reason just run
`docker-compose down && docker system prune -a && docker-compose up`.
This command will remove  any docker containers from the system and recreate them.