#!/bin/bash
echo "Running composer self-update"
docker exec -u www-data docker-php composer self-update
if [ $# -eq 0 ]
  then
    echo "Running composer install"
    docker exec docker-php mkdir -p /var/www/code/vendor
    docker exec docker-php chown -R www-data:www-data /var/www/code/vendor/
    docker exec -u www-data docker-php composer install
  else
    echo "Running composer $@"
    docker exec -u www-data docker-php composer "$@"
fi
