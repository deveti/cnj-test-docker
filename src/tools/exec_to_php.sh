#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "You are now in bash in a container."
	docker exec -it -u www-data docker-php //bin//sh
  else
    echo "Executing commmand $@ in a container."
	docker exec -it -u www-data docker-php "$@"
fi
